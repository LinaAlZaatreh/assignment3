using System.Drawing;
using System.Drawing.Imaging;

namespace Assignment3
{
    abstract class ImageConverter: IDisposable
    {
        private Bitmap _bitmap;
        private bool _disposed ;

        public ImageConverter(Bitmap bitmap)
        {
            this._bitmap = bitmap;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _bitmap.Dispose();
                }

                _disposed = true;
            }
        }

        ~ImageConverter()
        {
            Dispose(false);
        }
        
        static void Main(string[] args)
        {
            string directory = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));
            string imagePath = Path.Combine(directory, "parrots.jpeg");

            Bitmap originalImage = new Bitmap(imagePath);
            Bitmap image = CheckIfGrayscale(originalImage);
            
            Bitmap convertedImage = ConvertImage(image);

            convertedImage.Save("Converted.bmp", ImageFormat.Bmp);

            Console.WriteLine("Image converted successfully.");
            
            originalImage.Dispose();
            image.Dispose();
        }

        static Bitmap CheckIfGrayscale(Bitmap originalImage)
        {
            if (originalImage.PixelFormat == PixelFormat.Format8bppIndexed)
            {
                Console.WriteLine("The input image is already 8-bit grayscale.");
                return null;
            }
            
            if (originalImage.PixelFormat != PixelFormat.Format24bppRgb)
            {
                Console.WriteLine("The input image is not 24-bit RGB.");
                return null;
            }
            
            return originalImage;
        }

        static Bitmap ConvertImage(Bitmap image)
        {
            int width = image.Width;
            int height = image.Height;
            Bitmap convertedImage = new Bitmap(width, height, PixelFormat.Format8bppIndexed);

            ColorPalette palette = convertedImage.Palette;
            for (int i = 0; i < 256; i++)
            {
                palette.Entries[i] = Color.FromArgb(i, i, i);
            }

            convertedImage.Palette = palette;

            BitmapData convertedImageData = convertedImage.LockBits(
                new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly,
                PixelFormat.Format8bppIndexed);

            BitmapData originalImageData = image.LockBits(
                new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb);

            unsafe
            {
                byte* originalPtr = (byte*)originalImageData.Scan0;
                byte* convertedPtr = (byte*)convertedImageData.Scan0;
                
                int originalOffset= originalImageData.Stride - width * 3;
                int convertedOffset= convertedImageData.Stride - width;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        byte r = originalPtr[2];
                        byte g = originalPtr[1];
                        byte b = originalPtr[0];

                        byte gray = (byte)(0.3 * r + 0.59 * g + 0.11 * b);
                        convertedPtr[0] = (byte)gray;

                        originalPtr += 3;
                        convertedPtr += 1;
                    }

                    originalOffset++;
                    convertedOffset++;
                }
            }

            image.UnlockBits(originalImageData);
            convertedImage.UnlockBits(convertedImageData);
            image.Dispose();

            return convertedImage;
        }
    }
}
